# [Agwa](http://www.agwafarm.com)

Welcome to the Agwa Mobile Developer Home Assignment by Yehuda Cohen.  
And Let's Get Some Veggies in Your Basket! 🥒🍅🥬

## About:

This app was created using the [Flutter UI Toolkit by Google](http://flutter.dev/).
Barcodes have been created via [https://barcode.tec-it.com/](https://barcode.tec-it.com/).

## Scanning Instructions:

Press the top right "add to basket" button, followed by pressing the "Go on, Scan a Veggie!" on the next page.
Aim your device and scan one of the following barcodes:

![Tomatolicious](https://bitbucket.org/cohen72/agwa/raw/0667baa7fbeb430daff3dc4c92da2467ea2d3eeb/barcodes/1_Tomato.gif)||||||||||||
![Cucumberlicious](https://bitbucket.org/cohen72/agwa/raw/0667baa7fbeb430daff3dc4c92da2467ea2d3eeb/barcodes/2_Cucumber.gif)||||||||||||
![Lettucelicious](https://bitbucket.org/cohen72/agwa/raw/0667baa7fbeb430daff3dc4c92da2467ea2d3eeb/barcodes/3_Lettuce.gif)

- You can also access these barcode images from within the `barcodes` directory in the root project.
- To delete a veggie, just swipe left from within the basket page.

## Questions:
- Why did you choose the cross-platform you have chosen? What are the cons you might tackle because you chose it?
    - I decided to use the Flutter platform because first I didn't have experience with it and I felt it would
      be a good challenge for me. Second, I wanted to compare it to React Native which I've been using for
	  the last few months.  The cons about the platform are that it doesn't produce native components
	  and you can't exactly replicate the smooth feel of iOS.  However, I feel that it get's real close, and overall
	  I like Flutter better than React Native from my short experience with it. It's straightforward, stable, and a
	  better development experience.
	
- If you had to write unit tests and acceptance tests for your app, what are the main tests you would write?
    - Test to assert all CRUD operations operate properly within the vegetables database.
	   - Assert quantity is updated properly within the database when adding the same vegetable
	- Test to assert a barcode of a non-supported veggie will not be added to the basket.	

- If you had to add a feature to the app - what was it?
    - Detail Veggie Page - Drill down on each veggie to get to know your Veggie!
	    - Update quanities
		- Read more interesting information about the vegetable - Health Benefits, Best times to grow, etc.
	- Perhaps [MLKit from firebase](https://firebase.google.com/products/ml-kit),  
	  and use machine learning to recognize the vegetable using the camera (without a barcode).	

## Dependencies Used:
- Database - sqflite: 
- Location Permissions - location: 2.4.0
- Barcode Scanning - barcode_scan: 1.0.0
- Networking - http

## Open Weather:
Please note, that the API key for Open weather is hardcoded within the codebase, which is not advised.  
Normally we might want to inject this key via the build process on the CI through an environment file.

## TODO:
- Prevent user from scanning and adding items from bar codes that are not of the supported vegetable types.
- Allow re-ordering of the vegetables by 'long pressing' on the vegetable.
- Smoother transitions between welcome screen and main app.

## Tutorials Used:
- [Flutter & SQLite](https://medium.com/@suragch/simple-sqflite-database-example-in-flutter-e56a5aaa3f91)
- [Getting started with the BloC Pattern - Ray Wenderlich](https://www.raywenderlich.com/4074597-getting-started-with-the-bloc-pattern)

## More about Flutter and a few resources if you want to learn more:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
