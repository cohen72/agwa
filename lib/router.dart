import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'screens/home/homeScreen.dart';
import 'screens/welcome/welcomeScreen.dart';
import 'screens/barcode/barcodeScreen.dart';

const String HomeViewRoute = '/';
const String WelcomeViewRoute = 'welcome';
const String BarcodeScannerViewRoute = 'barcode';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomeViewRoute:
        return PageRouteBuilder(
            opaque: true,
            transitionDuration: const Duration(milliseconds: 300),
            pageBuilder: (BuildContext context, _, __) {
              return HomePageTabScaffold();
            },
            transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
              return FadeTransition(
                opacity: animation,
                child: child,
              );
            });

      case WelcomeViewRoute:
        return CupertinoPageRoute(builder: (_) => WelcomePage());
      case BarcodeScannerViewRoute:
        return CupertinoPageRoute(fullscreenDialog: true, builder: (_) => BarcodeScanScreen());
      default:
        return CupertinoPageRoute(
            builder: (_) => CupertinoPageScaffold(
                  child: Center(child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
