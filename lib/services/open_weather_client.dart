/*
 * Copyright (c) 2019 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import 'dart:async';
import 'dart:convert' show json;

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:location/location.dart';
import 'package:meta/meta.dart';

import '../models/forecast.dart';

class OpenWeatherClient {
  final _apiKey = '840c845f9bad37bba7b6582103d9ebc7';
  final _host = 'api.openweathermap.org';
  final _contextRoot = 'data/2.5';

  Future<List<Forecast>> fetchFiveDayDailyForecast(LocationData locationData) async {
    Map<String, String> parameters = {'q': 'haifa', 'units': 'metric'};

    if (locationData != null) {
      parameters = {
        'lat': locationData.latitude.toString(),
        'lon': locationData.longitude.toString(),
        'units': 'metric'
      };
    }
    final results = await request(path: 'forecast', parameters: parameters);
    if (results != null) {
      final list = results['list'];
      return list.map<Forecast>((json) => Forecast.fromJson(json)).toList(growable: false);
    } else {
      return null;
    }
  }

  Future<Map> request({@required String path, Map<String, String> parameters}) async {
    parameters['appid'] = _apiKey;
    final uri = Uri.https(_host, '$_contextRoot/$path', parameters);
    debugPrint(uri.toString());
    final results = await http.get(uri, headers: _headers);

    if (results.statusCode == 200) {
      final jsonObject = json.decode(results.body);
      return jsonObject;
    } else {
      return null;
    }
  }

  Map<String, String> get _headers => {'Accept': 'application/json', 'user-key': _apiKey};
}
