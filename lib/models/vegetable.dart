import 'package:agwa/models/model.dart';
import 'package:meta/meta.dart';

class Vegetable implements Model {
  static String table = 'veggies';

  int id;
  final String name;
  int quantity;

  Vegetable({@required this.name, this.quantity = 0});

  Vegetable.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        quantity = json['quantity'],
        id = json['id'];

  Map<String, dynamic> toJson() => {
        'name': name,
        'quantity': quantity,
        'id': id,
      };

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {'name': name, 'quantity': quantity};

    if (id != null) {
      map['id'] = id;
    }
    return map;
  }

  static Vegetable fromMap(Map<String, dynamic> map) {
    return Vegetable(name: map['name'], quantity: map['quantity']);
  }
}
