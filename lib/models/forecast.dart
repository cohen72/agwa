import 'package:intl/intl.dart';

class Forecast {
  final String weatherDescription;
  final String date;
  final int timestamp;

  Forecast.fromJson(json)
      : weatherDescription = json['weather'][0]['description'],
        timestamp = json['dt'],
        date = _formatDate(json['dt']);

  static String _formatDate(int timestamp) {
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    String formattedDate = DateFormat("MMM dd, yyyy").format(date);
    return formattedDate;
  }
}
