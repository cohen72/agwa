import 'package:agwa/bloc/weather_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'bloc/bloc_provider_tree.dart';
import 'bloc/bloc_provider.dart';
import 'bloc/basket_bloc.dart';
import 'router.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  return runApp(AgwaApp());
}

class AgwaApp extends StatelessWidget {
  final bool isFirstLaunch = false;

  @override
  Widget build(BuildContext context) {
    final bloc = BasketBloc();
    bloc.initialize();
    final weatherBloc = WeatherBloc();
    weatherBloc.getWeatherForecast();
    return BlocProviderTree(
      blocProviders: [
        BlocProvider<BasketBloc>(bloc: bloc),
        BlocProvider<WeatherBloc>(bloc: weatherBloc),
      ],
      child: CupertinoApp(
        localizationsDelegates: [
          DefaultMaterialLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate,
          DefaultWidgetsLocalizations.delegate,
        ],
        title: 'Agwa Farm App',
        theme: CupertinoThemeData(
          textTheme: CupertinoTextThemeData(textStyle: TextStyle(fontFamily: 'Comfortaa')),
          primaryColor: Colors.green,
        ),
        onGenerateRoute: Router.generateRoute,
        initialRoute: WelcomeViewRoute,
      ),
    );
  }
}
