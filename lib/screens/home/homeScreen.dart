import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../components/bottomNavBar.dart';
import '../cart/basketScreen.dart';
import '../weather/weatherScreen.dart';

class HomePageTabScaffold extends StatelessWidget {
  HomePageTabScaffold({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return CupertinoTabScaffold(
      tabBar: bottomNavigationBar(),
      tabBuilder: (BuildContext context, int index) {
        switch (index) {
          case 0:
            return BasketScreenTabView();
            break;
          case 1:
            return WeatherScreenTabView();
            break;
          default:
            return Container();
            break;
        }
      },
    );
  }
}
