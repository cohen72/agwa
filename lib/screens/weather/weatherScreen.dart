import 'package:agwa/bloc/weather_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../models/forecast.dart';
import '../../bloc/bloc_provider.dart';

class WeatherScreenTabView extends StatelessWidget {
  WeatherScreenTabView({Key key, this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
//    final bloc = WeatherBloc();
//    final bloc = BlocProvider.of<WeatherBloc>(context);
//    bloc.getWeatherForecast();
    return CupertinoTabView(
      builder: (BuildContext context) {
        return CupertinoPageScaffold(
          navigationBar: CupertinoNavigationBar(
            middle: Text('Weather'),
          ),
          child: Scaffold(
            body: WeatherListView(),
          ),
        );
      },
    );
  }
}

class WeatherListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<WeatherBloc>(context);
    return StreamBuilder<List<Forecast>>(
      stream: bloc.forecastStream,
      initialData: bloc.forecasts,
      builder: (context, snapshot) {
        final forecasts = snapshot.data;
        return SafeArea(
            child: forecasts.length == 0
                ? Center(child: Text('Fetching Weather!'))
                : ListView.separated(
                    padding: const EdgeInsets.all(8),
                    itemCount: forecasts.length,
                    itemBuilder: (BuildContext context, int index) {
                      Forecast item = forecasts[index];
                      return Container(
                        height: 100,
                        color: Colors.amberAccent,
                        child: ListTile(
                          title: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "${item.date}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text("${item.weatherDescription}")
                            ],
                          ),
                          key: ObjectKey(item.timestamp.toString()),
                          trailing: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[_generateWeatherIcon(item.weatherDescription)],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) => const Divider(),
                  ));
      },
    );
  }
}

Icon _generateWeatherIcon(String weatherDescription) {
  Icon icon;
  if (weatherDescription.contains("scattered clouds")) {
    icon = Icon(Icons.cloud);
  } else if (weatherDescription.contains("clear sky")) {
    icon = Icon(Icons.wb_sunny);
  } else if (weatherDescription.contains("broken clouds")) {
    icon = Icon(Icons.cloud_queue);
  } else if (weatherDescription.contains("overcast clouds")) {
    icon = Icon(Icons.cloud_off);
  } else if (weatherDescription.contains("few clouds")) {
    icon = Icon(Icons.cloud_circle);
  } else {
    icon = Icon(Icons.snooze);
  }
  return icon;
}
