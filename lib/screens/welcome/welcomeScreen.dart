import 'package:agwa/services/db.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../router.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/basket_bloc.dart';

class WelcomePage extends StatelessWidget {
  WelcomePage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<BasketBloc>(context);
    return BlocProvider<BasketBloc>(
        bloc: bloc,
        child: CupertinoPageScaffold(
            child: Container(
          padding: EdgeInsets.all(24.0),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/launcher/icon.png',
                width: 300,
              ),
              SizedBox(
                width: double.infinity,
                child: CupertinoButton(
                  color: Colors.green,
                  padding: EdgeInsets.all(8.0),
                  child: Text('New Basket'),
                  onPressed: () {
                    bloc.clearVeggies();
                    Navigator.of(context).pushReplacementNamed(HomeViewRoute);
                  },
                ),
              ),
              Padding(padding: EdgeInsets.all(8.0)),
              SizedBox(
                width: double.infinity,
                child: CupertinoButton(
                  color: Colors.greenAccent,
                  child: Text('Continue Basket'),
                  onPressed: () {
                    Navigator.of(context).pushReplacementNamed(HomeViewRoute);
                  },
                ),
              )
            ],
          )),
        )));
  }
}
