import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/basket_bloc.dart';
import '../../models/vegetable.dart';

class _BarcodeScanState extends State<BarcodeScanScreen> {
  String barcode = "";
  bool isBarcodeValid = false;
  final textQuantityController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<BasketBloc>(context);
    return BlocProvider<BasketBloc>(
        bloc: bloc,
        child: CupertinoPageScaffold(
            navigationBar: CupertinoNavigationBar(
              middle: Text('Veggie Code Scanner'),
            ),
            child: SafeArea(
              child: Container(
                  padding: const EdgeInsets.all(20),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(30.0),
                        ),
                        CupertinoButton(
                          color: Colors.green,
                          padding: EdgeInsets.all(8.0),
                          child: const Text('Go on, Scan a Veggie!'),
                          onPressed: _scan,
                        ),
                        Container(
                            padding: const EdgeInsets.all(20),
                            width: 200,
                            child: this.isBarcodeValid
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Expanded(
                                        child: CupertinoTextField(
                                          placeholder: 'Qty',
                                          style: TextStyle(color: Colors.black),
                                          keyboardType: TextInputType.number,
                                          enabled: this.isBarcodeValid,
                                          controller: textQuantityController,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.all(8.0),
                                      ),
                                      CupertinoButton(
                                        color: Colors.green,
                                        padding: EdgeInsets.all(8.0),
                                        child: const Text('ADD'),
                                        disabledColor: Colors.green[100],
                                        onPressed: this.isBarcodeValid ? () => _addVegetable(bloc) : null,
                                      )
                                    ],
                                  )
                                : null),
                      ],
                    ),
                  )),
            )));
  }

  void _addVegetable(BasketBloc bloc) {
    String qty = this.textQuantityController.text;
    Vegetable vegetable = new Vegetable(name: this.barcode, quantity: int.parse(qty));
    bloc.addVegetable(vegetable);
    debugPrint(bloc.vegetables.length.toString());
    Navigator.of(context).pop();
  }

  Future _scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() {
        this.barcode = barcode;
        this.isBarcodeValid = true;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'Camera permission not granted';
          this.isBarcodeValid = false;
        });
      } else {
        setState(() {
          this.barcode = 'unknown error: ${e.details.toString()}';
          this.isBarcodeValid = false;
        });
      }
    } on FormatException {
      setState(() {
        this.barcode = 'null (User returned using the "back" button before scanning anything.';
        this.isBarcodeValid = false;
      });
    } catch (e) {
      setState(() {
        this.barcode = 'unknown error: ${e.details.toString()}';
        this.isBarcodeValid = false;
      });
    }
  }

  @override
  void dispose() {
    textQuantityController.dispose();
    super.dispose();
  }
}

class BarcodeScanScreen extends StatefulWidget {
  @override
  _BarcodeScanState createState() {
    return new _BarcodeScanState();
  }
}
