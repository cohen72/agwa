import 'package:agwa/models/vegetable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../router.dart';
import '../../bloc/bloc_provider.dart';
import '../../bloc/basket_bloc.dart';

class BasketScreenTabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoTabView(
      builder: (BuildContext context) {
        return CupertinoPageScaffold(
            navigationBar: CupertinoNavigationBar(
              middle: Text('Cart'),
              trailing: GestureDetector(
                onTap: () {
                  Navigator.of(context, rootNavigator: true).pushNamed(BarcodeScannerViewRoute);
                },
                child: Icon(Icons.add_shopping_cart),
              ),
            ),
            child: Scaffold(body: BasketListView()));
      },
    );
  }
}

class BasketListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<BasketBloc>(context);

    return StreamBuilder<List<Vegetable>>(
      stream: bloc.vegetableStream,
      initialData: bloc.vegetables,
      builder: (context, snapshot) {
        final vegetables = snapshot.data;

        return SafeArea(
            child: Center(
                child: (vegetables != null && vegetables.length > 0)
                    ? ListView.separated(
                        padding: const EdgeInsets.all(8),
                        itemCount: vegetables.length,
                        itemBuilder: (BuildContext context, int index) {
                          Vegetable item = vegetables[index];

                          return Dismissible(
                            key: ObjectKey(item.id),
                            direction: DismissDirection.endToStart,
                            background: stackBehindDismiss(),
                            child: Container(
                                height: 50,
                                color: Colors.lime,
                                child: ListTile(
                                  title: Text("${item.name}"),
                                  trailing: Text("QTY: ${item.quantity}"),
                                )),
                            onDismissed: (direction) {
                              bloc.deleteVegetable(item);
                            },
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) => const Divider(),
                      )
                    : Center(child: Text('Go on, "+" some Veggies!'))));
      },
    );
  }
}

Widget stackBehindDismiss() {
  return Container(
    alignment: Alignment.centerRight,
    padding: EdgeInsets.only(right: 20.0),
    color: Colors.red,
    child: Icon(
      Icons.delete,
      color: Colors.white,
    ),
  );
}
