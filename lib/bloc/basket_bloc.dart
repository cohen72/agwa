import 'dart:async';
import 'package:agwa/services/db.dart';
import 'package:flutter/cupertino.dart';
import 'bloc.dart';
import '../models/vegetable.dart';

const String veggieKey = 'veggies';

class BasketBloc implements Bloc {
  List<Vegetable> _vegetables = [];
  List<Vegetable> get vegetables => _vegetables;
  final dbHelper = DatabaseHelper.instance;

  void initialize() async {
    final allRows = await dbHelper.queryAllRows();
    _vegetables = allRows.map((item) => Vegetable.fromMap(item)).toList();
    if (_vegetables == null) {
      _vegetables = [];
    }
    _sink();
  }

  final _vegetableController = StreamController<List<Vegetable>>();

  Stream<List<Vegetable>> get vegetableStream => _vegetableController.stream;

  void clearVeggies() {
    DatabaseHelper.instance.deleteAll();
    _sink();
  }

  void deleteVegetable(Vegetable vegetable) {
    dbHelper.delete(vegetable.name);
    _sink();
  }

  void addVegetable(Vegetable newVeggie) async {
    final existingVegetable = await dbHelper.query(newVeggie.toMap());
    if (existingVegetable.length > 0) {
      Vegetable existingVeggie = Vegetable.fromMap(existingVegetable[0]);
      newVeggie.quantity = newVeggie.quantity + existingVeggie.quantity;
      dbHelper.update(newVeggie.toMap());
    } else {
      dbHelper.insert(newVeggie.toMap());
    }
    _sink();
  }

  void _sink() async {
    final allRows = await dbHelper.queryAllRows();
    _vegetables = allRows.map((item) => Vegetable.fromMap(item)).toList();
    _vegetableController.sink.add(_vegetables);
  }

  /* TODO: Implement DB functionality to reorder vegetables based on separate "Order" Table
  void reorder(int oldIndex, int newIndex) {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    Vegetable vegetable = _vegetables.removeAt(oldIndex);
    _vegetables.insert(newIndex, vegetable);//
  }
  */

  @override
  void dispose() {
    _vegetableController.close();
  }
}
