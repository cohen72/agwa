import 'dart:async';
import 'package:agwa/services/open_weather_client.dart';
import 'package:flutter/cupertino.dart';
import 'bloc.dart';
import '../models/forecast.dart';
import 'package:location/location.dart';

class WeatherBloc implements Bloc {
  List<Forecast> _forecasts = [];
  final _client = OpenWeatherClient();
  List<Forecast> get forecasts => _forecasts;

  void initialize() async {}

  final _forecastController = StreamController<List<Forecast>>();

  Stream<List<Forecast>> get forecastStream => _forecastController.stream;

  void getWeatherForecast() async {
    var location = new Location();
    if (await location.hasPermission()) {
      await getLocation();
    } else {
      await Location().requestPermission();
      await getLocation();
    }
  }

  Future getLocation() async {
    try {
      LocationData currentLocation = await Location().getLocation();
      _fetchLocation(currentLocation);
    } catch (e) {
      debugPrint(e.toString());
      _fetchLocation(null);
    }
  }

  void _fetchLocation(LocationData location) async {
    List<Forecast> results = await _client.fetchFiveDayDailyForecast(location);
    List<Forecast> filter = [];
    for (var i = 0; i < results.length; i++) {
      // since we receive 8 forecasts per day (every 3 hours), let's get every 8th one.
      if (i % 8 == 0) {
        debugPrint(results[i].date);
        filter.add(results[i]);
      }
    }
    _forecasts = filter;
    _forecastController.sink.add(_forecasts);
  }

  @override
  void dispose() {
    _forecastController.close();
  }
}
