import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget bottomNavigationBar()=> CupertinoTabBar(
  iconSize: 30,
//  selectedFontSize: 14.0,
//  unselectedFontSize: 14.0,
  items: [
    BottomNavigationBarItem(
      icon: Icon(
        Icons.shopping_basket,
        size: 20,
        ),
      title: Text(
        "my basket",
        style: TextStyle(fontSize: 15),
        ),
      ),
    BottomNavigationBarItem(
      icon: Icon(
        Icons.wb_sunny,
        size: 20,
        ),
      title: Text(
        "weather",
        style: TextStyle(fontSize: 15),
        ),
      )
  ],
  );